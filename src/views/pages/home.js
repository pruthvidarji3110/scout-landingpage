// import external modules
import React, { Component } from 'react';
// import { loginApi } from '../../redux/actions/api/apiAction';
// import { history } from '../../app/AppRouter';
// import { connect } from 'react-redux';
// import {Button} from "reactstrap"
import Header from '../../components/navbar/header'
import Footer from '../../components/footer/footer'
import Slider from '../../components/slider/slider'
import ImgSlider from '../../components/slider/imgSlider'
import { Button } from 'reactstrap';
import BannerImg from "../../assets/images/banner-img.svg"
import MobileImg from "../../assets/images/mobile-img.png"
import ListIcon1 from "../../assets/images/list-icon-1.svg"
import ListIcon2 from "../../assets/images/list-icon-2.svg"
import ListIcon3 from "../../assets/images/list-icon-3.svg"
import CrossUser from "../../assets/images/scross-user.svg"
import GoalSetting from "../../assets/images/goal-setting.png"
import GoogleStore from "../../assets/images/google-apps-store.svg"
import IphoneStore from "../../assets/images/iphone-apps-store.svg"


class Home extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       email: '',
//       password: '',
//       disable: false,
//       emailError: '',
//       passwordError: '',
//       showPassword: false,
//     };
//   }

//   loginClick = async () => {
//     console.log('i am called');
//     history.push('/forgotPassword');
//   };

  render() {
    return (
      <div>
          {/* <span className="vector-img">
          <svg width="1320" height="980" viewBox="0 0 1320 940" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M267.112 -207.245C159.23 -128.714 73.9911 -23.6899 30.3725 98.3649C-65.8548 369.282 74.6569 723.777 378.988 807.355C663.675 885.255 989.316 720.623 1040.93 436.775C1068.89 281.289 1090.54 131.481 1259.35 58.6262C1293.65 43.8029 1330.61 33.7106 1363.9 16.6797C1546.04 -76.6748 1265.35 -211.976 1184.1 -252.03C999.638 -343.492 783.21 -382.285 578.102 -342.231C465.893 -320.469 358.344 -273.476 267.112 -207.245Z" fill="#DBF7EF"/>
          </svg>
          </span> */}
        <Header />
        <section id="banner">
            <div className="container-fluid">
                <div className="inner-container">
                    <div className="row">
                        <div className="banner-txt-sec col-md-6">
                            <div className="banner-txt-main">
                                <h1 className="main-head">
                                    Scout Dental <br/>Practice Analytics
                                </h1>
                                <p>
                                    Scout is the Dental analytics app that gives you everything you need to know about your practice on your phone.
                                </p>
                                <Button color="primary">SIGN UP NOW</Button>
                            </div>
                        </div>
                        <div className="col-md-6 banner-img-sec">
                            <img src={BannerImg}  alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="dental-analytics">
          <div className="main-container">
            <div className="dental-analytics-row">
                <div className="img-sec">
                    <img src={MobileImg}  alt=""/>
                </div>
                <div className="txt-sec">
                    <h2 className="head-txt">Dental Analytics</h2>
                    <p>Scout is a mobile dental analytics app for your phone. Simple,
                        easy to visualize, actionable KPIs available instantly. No more
                        spending hours sorting through hundreds of KPIs that no one
                        understands.</p>
                    <h3 className="inner-head-txt">Scout Analytics</h3>
                    <div className="icon-list-sec">
                        <div className="single-icon-list">
                            <div className="icon-sec">
                                <img src={ListIcon1}  alt=""/>
                            </div>
                            <div className="list-txt">
                                Set goals for the most important KPIs for
                                your practice and watch your practice grow.
                            </div>
                        </div>
                        <div className="single-icon-list">
                            <div className="icon-sec">
                                <img src={ListIcon2}  alt=""/>
                            </div>
                            <div className="list-txt">
                            Compare KPIs with other practices across the 
                            country to understand your strengths and weaknesses.
                            </div>
                        </div>
                        <div className="single-icon-list">
                            <div className="icon-sec">
                                <img src={ListIcon3}  alt=""/>
                            </div>
                            <div className="list-txt">
                                Easy to use. No need to spend hours on 
                                training or staring at a complicated dashboard.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </section>

        <section id="cross-user">
            <div className="main-inner-container">
                <div className="cross-user-row right-img-sec">
                    <div className="txt-sec">
                        <h2 className="head-txt">Cross-User Comparision</h2>
                        <p>Compare your individual practice KPIs to the average of all Scout users</p>
                        <p>This helps you know your strengths and weaknesses</p>
                        <p>Allows you to narrow your focus to specific KPIs that you need to work on</p>
                    </div>
                    <div className="img-sec">
                        <img src={CrossUser}  alt="Cross-User Comparision"/>
                    </div>
                </div>
            </div>
        </section>

        <section id="goal-setting">
            <div className="main-inner-container">
                <div className="cross-user-row left-img-sec">
                    <div className="img-sec">
                        <img src={GoalSetting}  alt="Goal Setting"/>
                    </div>
                    <div className="txt-sec">
                        <h2 className="head-txt">Goal Setting</h2>
                        <p>Set goals for each individual KPI</p>
                        <p>Track your goal progress on a daily, weekly, monthly or yearly basis</p>
                        <p>Focus on the KPIs that actually result in practice growth</p>
                    </div>
                </div>
            </div>
        </section>

        <section id="download-apps">
            <div className="main-inner-container">
                <h3 className="white-head">
                    Download app Lorem ipsum dummy text
                </h3>
                <div className="apps-sec">
                        <div className="apps-store google-store">
                            <a href="#" title="Our app is now available on Google Play" target="_blank"
                                rel="noopener noreferrer">
                                <img src={GoogleStore}  alt="Google Play Store"/>
                            </a>
                        </div>
                        <div className="apps-store iphone-store">
                        <a href="#" title="Our app is now available on Iphone Apps Store" target="_blank"
                                rel="noopener noreferrer">
                                <img src={IphoneStore}  alt="Iphone Apps Store"/>
                            </a>
                        </div>
                </div>
            </div>
        </section>

        <section id="key-performance">
            <div className="main-inner-container">
                <div className="key-performance-row right-img-sec">
                    <div className="txt-sec">
                        <h2 className="head-txt">Key Performance Indicator</h2>
                        <p>20 Important KPIs selected. Each KPI is compared  to all Scout users across the country 
                            and you can set individual goals.This comparision combined with goal setting will allow 
                            you to get actual results in your practice.</p>
                        <Slider />
                    </div>
                    <div className="img-sec">
                        {/* <img src={CrossUser}  alt="Cross-User Comparision"/> */}
                        <ImgSlider />
                    </div>
                </div>
            </div>
        </section>

        <section id="pricing">
            <div className="main-inner-container">
                <h2 className="head-txt">Pricing</h2>
                <p>
                    Free trial of the pro version available for open Dental Users
                </p>
                <div className="price-row">
                    <div className="price-col basic-price">
                        <h4 className="price-head">BASIC</h4>
                        <span className="price">FREE for Open Dental</span>
                        <p className="price-info">
                            Lerem ipsum dummy text used for display purpose
                        </p>
                        <ul className="price-list">
                            <li>        
                                <input className="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1" defaultChecked />
                                <label htmlFor="styled-checkbox-1">Dummy text display purpose here</label>
                            </li>
                            <li>        
                                <input className="styled-checkbox" id="styled-checkbox-2" type="checkbox" value="value1" defaultChecked />
                                <label htmlFor="styled-checkbox-2">Dummy text display purpose here</label>
                            </li>
                            <li>        
                                <input className="styled-checkbox" id="styled-checkbox-3" type="checkbox" value="value1" defaultChecked />
                                <label htmlFor="styled-checkbox-3">Dummy text display purpose here</label>
                            </li>
                            <li className="disable-check">        
                                <input className="styled-checkbox" id="styled-checkbox-4" type="checkbox" value="value1" defaultChecked />
                                <label htmlFor="styled-checkbox-4">Dummy text display purpose here</label>
                            </li>
                            <li className="disable-check">        
                                <input className="styled-checkbox" id="styled-checkbox-5" type="checkbox" value="value1" defaultChecked />
                                <label htmlFor="styled-checkbox-5">Dummy text display purpose here</label>
                            </li>
                            <li className="disable-check">        
                                <input className="styled-checkbox" id="styled-checkbox-6" type="checkbox" value="value1" defaultChecked />
                                <label htmlFor="styled-checkbox-6">Dummy text display purpose here</label>
                            </li>
                        </ul>
                        <Button color="primary">Get Started</Button>
                    </div>
                    <div className="price-col pro-price">
                        <h4 className="price-head">PRO</h4>
                        <span className="price"><span className="value">$49</span> / Per month</span>
                        <p className="price-info">
                            Lerem ipsum dummy text used for display purpose
                        </p>
                        <ul  className="price-list">
                            <li>        
                                <input className="styled-checkbox" id="styled-checkbox-7" type="checkbox" value="value1" defaultChecked />
                                <label htmlFor="styled-checkbox-7">Dummy text display purpose here</label>
                            </li>
                            <li>        
                                <input className="styled-checkbox" id="styled-checkbox-8" type="checkbox" value="value1" defaultChecked />
                                <label htmlFor="styled-checkbox-8">Dummy text display purpose here</label>
                            </li>
                            <li>        
                                <input className="styled-checkbox" id="styled-checkbox-9" type="checkbox" value="value1" defaultChecked />
                                <label htmlFor="styled-checkbox-9">Dummy text display purpose here</label>
                            </li>
                            <li>        
                                <input className="styled-checkbox" id="styled-checkbox-10" type="checkbox" value="value1" defaultChecked />
                                <label htmlFor="styled-checkbox-10">Dummy text display purpose here</label>
                            </li>
                            <li>        
                                <input className="styled-checkbox" id="styled-checkbox-11" type="checkbox" value="value1" defaultChecked />
                                <label htmlFor="styled-checkbox-11">Dummy text display purpose here</label>
                            </li>
                            <li>        
                                <input className="styled-checkbox" id="styled-checkbox-12" type="checkbox" value="value1" defaultChecked />
                                <label htmlFor="styled-checkbox-12">Dummy text display purpose here</label>
                            </li>
                        </ul>
                        <Button color="primary">Get Started</Button>
                    </div>
                </div>
            </div>
        </section>

        <Footer />
      </div>
    );
  }
}

// const mapStateToProps = (state) => {
//   const {} = state.login;
//   return {};
// };
export default Home;
