// import external modules
import React, { Component } from 'react';
import { loginApi } from '../../redux/actions/api/apiAction';
import { history } from '../../app/AppRouter';
import { connect } from 'react-redux';
import {Button} from "reactstrap"

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      disable: false,
      emailError: '',
      passwordError: '',
      showPassword: false,
    };
  }

  loginClick = async () => {
    console.log('i am called');
    history.push('/forgotPassword');
  };

  render() {
    return (
      <div>
        <input type='text' placeholder='Email'></input>
        <input type='password' placeholder='Password'></input>

        <button onClick={this.loginClick}>submit</button>
        <Button color="danger">save</Button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const {} = state.login;
  return {};
};
export default connect(mapStateToProps, { loginApi })(Login);
