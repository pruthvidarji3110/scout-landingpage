import { setLoginUser, setLoginFlag } from '../login/loginAction';
import { api } from '../../../api/api';
import { history } from '../../../app/AppRouter';

export const loginApi = (value) => {
  return async (dispatch, store) => {
    // dispatch(setloader(true));
    await api('auth/login', value, 'postWithoutToken')
      .then((res) => {
        if (res.status === 400) {
          // toastr.error(res.data.error);
        } else {
          dispatch(setLoginUser(res.data.data));
          localStorage.setItem('user', JSON.stringify(res.data.data));
          dispatch(setLoginFlag(true));
          history.push('/dashboard');
        }
      })
      .catch((err) => {});
  };
};
