import React from 'react';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';

const PublicRoute = ({ isAuthenticated, component: Component, ...rest }) => (
  <Route {...rest} component={(props) => <Component {...props} />} />
);

const mapStateToProps = (state) => {
  const isAuthenticated = state.login.loginFlag;
  return { isAuthenticated };
};

export default connect(mapStateToProps)(PublicRoute);
