// import external modules
import React, { lazy } from 'react';
import { Router, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute.js';
const createBrowserHistory = require('history').createBrowserHistory;

const LazyLogin = lazy(() => import('../views/pages/login'));
const LazyHome = lazy(() => import('../views/pages/home'));
const LazyForgotPassword = lazy(() => import('../views/pages/forgotPassword'));
const LazyReSetPassword = lazy(() => import('../views/pages/resetPassword'));
export const history = createBrowserHistory({ basename: '/scout-landingpage' });

class AppRouter extends React.Component {
  componentDidMount = async () => {};

  render() {
    return (
      <Router history={history}>
        <div>
          <Switch>
            <PublicRoute path='/login' component={LazyLogin} exact={true} />
            <PublicRoute
              path='/forgotPassword'
              component={LazyForgotPassword}
              exact={true}
            />
            <PublicRoute
              path='/reset-password'
              component={LazyReSetPassword}
              exact={true}
            />
            <PublicRoute path='/' component={LazyHome} exact={true} />
          </Switch>
        </div>
      </Router>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps, {})(AppRouter);
