import React, { Component, useState } from 'react';
import FooterGoogleStore from "../../assets/images/footer-play-store.svg"
import FooterIphoneStore from "../../assets/images/footer-apps-store.svg"
import FooterLogo from "../../assets/images/footer-logo.svg"
import Facebook from "../../assets/images/facebook.svg"
import Instagram from "../../assets/images/instagram.svg"
import Twitter from "../../assets/images/twitter.svg"
import LinkIn from "../../assets/images/linkedin.svg"


class Footer extends Component{
    render() {
        return (
                <div>
                    <footer id="footer">
                        <div className="main-inner-container">
                            <div className="footer-row">
                                <div className="footer-col about-sec">
                                    <a className="footer-logo" href="#" title="Scout Dental Practice Analytics" target="_blank"
                                        rel="noopener noreferrer">
                                        <img src={FooterLogo} height="50" alt="Scout Dental Practice Analytics"/>
                                    </a>
                                    <p className="bold-txt">Lorem Ipsum is simply dummy text
                                        of the printing and typesetting industry.</p>
                                    <p className="light-txt">
                                        Lorem Ipsum is simply dummy text of the
                                        printing and typesetting industry. 
                                    </p>
                                    <div className="apps-sec">
                                        <div className="apps-store iphone-store">
                                            <a href="#" title="Our app is now available on Iphone Apps Store" target="_blank"
                                            rel="noopener noreferrer">
                                                <img src={FooterIphoneStore}  alt="Iphone Apps Store"/>
                                            </a>
                                        </div>
                                        <div className="apps-store google-store">
                                            <a href="#" title="Our app is now available on Google Play" target="_blank"
                                                rel="noopener noreferrer">
                                                <img src={FooterGoogleStore}  alt="Google Play Store"/>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div className="footer-col explore-sec">
                                    <h6 className="footer-head">EXPLORE</h6>
                                    <div className="link-list">
                                        <a href="#" title="About Us">About us</a>
                                        <a href="#" title="Key Performance">Key Performance</a>
                                        <a href="#" title="Pricing">Pricing</a>
                                    </div>
                                </div>
                                <div className="footer-col company-sec">
                                    <h6 className="footer-head">COMPANY</h6>
                                    <div className="link-list">
                                        <a href="#" title="PRIVACY POLICY">PRIVACY POLICY</a>
                                        <a href="#" title="TERMS & CONDITIONS">TERMS & CONDITIONS</a>
                                    </div>
                                </div>
                                <div className="footer-col social-sec">
                                    <h6 className="footer-head">FOLLOW US</h6>
                                    <div className="social-link">
                                            <a href="#" title="Follow Us on Facebook" target="_blank"
                                                rel="noopener noreferrer">
                                                <img src={Facebook}  alt="Facebook"/>
                                            </a>
                                            <a href="#" title="Follow Us on Instagram" target="_blank"
                                                rel="noopener noreferrer">
                                                <img src={Instagram}  alt="Instagram"/>
                                            </a>
                                            <a href="#" title="Follow Us on Twitter" target="_blank"
                                                rel="noopener noreferrer">
                                                <img src={Twitter}  alt="Twitter"/>
                                            </a>
                                            <a href="#" title="Follow Us on LinkIn" target="_blank"
                                                rel="noopener noreferrer">
                                                <img src={LinkIn}  alt="LinkIn"/>
                                            </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>

                    <section id="bottom-footer">
                        <div className="main-inner-container">
                            <p>2021 © scout analytics. All rights reserved</p>
                        </div>
                    </section>
                </div>
                    
        );
    }
}

export default Footer;