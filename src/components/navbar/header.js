import React, { Component, useState } from 'react';
import logo from "../../assets/images/logo.svg"
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    NavbarText, Button
  } from 'reactstrap';

export default function Header(){
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);
    
        return (
            <header id="header">
                {/* <div className="container">
                    <div className="row"> */}
            <div className="navbar-wrapper">
                <div className="navbar-container content">        
                    <Navbar  expand="md">
                        <NavbarBrand href="/">
                            {/* <a href="#" title="Scout Dental Practice Analytics" target="_blank"
                            rel="noopener noreferrer"> */}
                                <img src={logo} height="50" alt="Scout Dental Practice Analytics"/>
                            {/* </a> */}
                        </NavbarBrand>
                        <NavbarToggler onClick={toggle}/>
                        <Collapse isOpen={isOpen}  navbar>
                    {/* <NavbarToggler onClick={toggle} />
                    <Collapse isOpen={isOpen} navbar> */}
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink href="#">About us</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#">key performance</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#">Pricing</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#">
                                    <Button color="primary">SIGN UP NOW</Button>
                                </NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
                    </div>
                </div>
            </header>
        );
    
}

