import React from 'react'
import OwlCarousel from 'react-owl-carousel'
import 'owl.carousel/dist/assets/owl.carousel.css'
import 'owl.carousel/dist/assets/owl.theme.default.css'
import ProductionPhone from "../../assets/images/production-img.png"


export default function ImgSlider() {
    const sliderResponsive = {
        0: {
            items: 1,
            nav: true,
            dots: false,
            autoplay: true,
            slideBy: 1,
            autoplayTimeout: 6000,
        },
    }
    const options = {
        // nav: true,
        // rewind: true,
        // autoplay: true,
        // slideBy: 1,
        // dots: false,
      };
    return (
            <OwlCarousel className='owl-theme' loop margin={10} responsive={sliderResponsive} options={options}>
                <div className="item">
                    <div className="single-img-sec">
                        <img src={ProductionPhone}  alt="Production"/>
                    </div>
                </div>
                <div className="item">
                    <div className="single-img-sec">
                        <img src={ProductionPhone}  alt="Production"/>
                    </div>
                </div>
                <div className="item">
                    <div className="single-img-sec">
                        <img src={ProductionPhone}  alt="Production"/>
                    </div>
                </div>
                <div className="item">
                    <div className="single-img-sec">
                        <img src={ProductionPhone}  alt="Production"/>
                    </div>
                </div>   
            </OwlCarousel>
    )
}
