import React from 'react'
import OwlCarousel from 'react-owl-carousel'
import 'owl.carousel/dist/assets/owl.carousel.css'
import 'owl.carousel/dist/assets/owl.theme.default.css'
import ProductionLogo from "../../assets/images/production-logo.svg"


export default function Slider() {
    const sliderResponsive = {
        0: {
            items: 1,
            autoplay: true,
            slideBy: 1,
            autoplayTimeout: 6000,
        },
    }
    return (
            <OwlCarousel className='owl-theme' loop margin={10} responsive={sliderResponsive}>
                <div className="item">
                    <div className="single-item">
                        <div className="logo-sec">
                            <img src={ProductionLogo}  alt="Production"/>
                        </div>
                        <h5 className="slider-head">Production</h5>
                        <p>Production per new patient, Production per hygiene exam, same day production</p>
                    </div>
                </div>
                <div className="item">
                    <div className="single-item">
                        <div className="logo-sec">
                            <img src={ProductionLogo}  alt="Production"/>
                        </div>
                        <h5 className="slider-head">Production</h5>
                        <p>Production per new patient, Production per hygiene exam, same day production</p>
                    </div>
                </div>
                <div className="item">
                    <div className="single-item">
                        <div className="logo-sec">
                            <img src={ProductionLogo}  alt="Production"/>
                        </div>
                        <h5 className="slider-head">Production</h5>
                        <p>Production per new patient, Production per hygiene exam, same day production</p>
                    </div>
                </div>
                <div className="item">
                    <div className="single-item">
                        <div className="logo-sec">
                            <img src={ProductionLogo}  alt="Production"/>
                        </div>
                        <h5 className="slider-head">Production</h5>
                        <p>Production per new patient, Production per hygiene exam, same day production</p>
                    </div>
                </div>   
                
            </OwlCarousel>
    )
}
