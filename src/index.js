import * as serviceWorker from './serviceWorker';
import React, { Suspense, lazy } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './redux/storeConfig/store';
import { setLoginFlag, setLoginUser } from './redux/actions/login/loginAction';

import './index.scss';

import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/scss/style.css';
import { history } from './app/AppRouter';
import Spinner from './components/spinner/spinner';
const LazyApp = lazy(() => import('./app/AppRouter'));

const jsx = (
  <Provider store={store}>
    <Suspense fallback={<Spinner />}>
      <LazyApp />
    </Suspense>
  </Provider>
);

let hasRendered = false;
const renderApp = async () => {
  // if (!hasRendered) {
    ReactDOM.render(jsx, document.getElementById('root'));
    hasRendered = true;
  // }
};

// serviceWorker.unregister();

const renderLogin = () => {
  store.dispatch(setLoginFlag(false));
  renderApp();

  // if (
  //   // window.location.pathname === '/reset-password' ||
  //   // window.location.pathname === '/reset-password'
  // ) {
  // } else {
  //   // history.push('/login');
  // }
};

try {
  if (
    window.location.pathname === '/reset-password' ||
    window.location.pathname === '/reset-password'
  ) {
    localStorage.removeItem('user');
    renderApp();
  }
  const login = localStorage.getItem('user');
  if (login) {
    const loginObject = JSON.parse(login);
    store.dispatch(setLoginUser(loginObject));
    store.dispatch(setLoginFlag(true));
    renderApp();
    if (window.location.pathname === '/') {
        history.push('/home');
    }
  } else {
    renderLogin();
  }
} catch (e) {
  renderLogin();
}
